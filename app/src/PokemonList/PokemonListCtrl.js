'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, PokemonsService, BerriesService, $location, $rootScope, $q) {

    // PokemonsService.getPokemons().then(function(response) {
    //     $scope.pokemons = response.data.results;
    // });
    //
    // BerriesService.getBerries().then(function(response) {
    //     $scope.berries = response.data.results;
    // });
    $scope.listPokemonsLoaded = false;
    $scope.listBerriesLoaded = false;

    $q.all([
      PokemonsService.getPokemons(), BerriesService.getBerries()
    ])
      .then(function(response) {
        $scope.pokemons = response[0].data.data;
        $scope.listPokemonsLoaded = true;

        //$scope.berries = response.data.result;  // для web сервиса pokeapi.co/api/v2/pokemon
        $scope.berries = response[1].data.data;
        $scope.listBerriesLoaded = true;
    });

    $scope.runEdit = function (pokemon) {

        $scope.$parent.$broadcast('pro', {  // как передать в controller EditPokemonCtrl инфо отсюда?
            name: pokemon
        });

        $location.url('/edit/' + pokemon.name).search({id: pokemon.objectId});

    };


    $scope.toggle = function (event) {  // http://javascript.ru/forum/angular/44335-vlozhennyjj-ng-click.html
        //console.log(event.target.tagName);
        //console.log(event.currentTarget);

        if(event.target.tagName === 'H4'){
            return;
        }

        event.preventDefault();
    };

});
