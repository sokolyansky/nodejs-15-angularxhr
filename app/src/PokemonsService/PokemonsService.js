angular
    .module('PokemonApp')
    /*.config(['$httpProvider', function ($httpProvider) { // можно и так (9 строка)
     $httpProvider.defaults.headers.post["application-id"] = '9D89774E-2150-9711-FFE7-45638A3D2700';
     $httpProvider.defaults.headers.post["secret-key"] = '1C659344-7263-0CE6-FF4A-0F503D73CB00';
     }])*/
    .factory('PokemonsService', function($http) {

            $http.defaults.headers.common = {
                "application-id": '9D89774E-2150-9711-FFE7-45638A3D2700',
                "secret-key": '1C659344-7263-0CE6-FF4A-0F503D73CB00'
            };

            return {

                getPokemons: function() {
                    //return $http.get('http://pokeapi.co/api/v2/pokemon/?limit=10');
                    return $http.get('https://api.backendless.com/v1/data/pokemon');
                },

                getPokemon: function(pokemonId) {
                    //return $http.get('http://pokeapi.co/api/v2/pokemon/' + pokemonId);
                    return $http.get('https://api.backendless.com/v1/data/pokemon/' + pokemonId);
                },

                createPokemon: function(pokemonData) {
                    return $http({
                        method: 'POST',
                        url: 'https://api.backendless.com/v1/data/pokemon',
                        data: pokemonData
                    });
                },

                deletePokemon: function(pokemonId) {
                    return $http({
                        method: 'DELETE',
                        url: 'https://api.backendless.com/v1/data/pokemon/' + pokemonId
                    });
                },
                editPokemon: function (myPokemon) {
                    //console.log(myPokemon.weight);
                    return $http({
                        method: 'PUT',
                        url: 'https://api.backendless.com/v1/data/pokemon/' + myPokemon.objectId,
                        data: {
                            name: myPokemon.name,
                            weight: myPokemon.weight
                        }
                    })
                }
            }

        }

    );
